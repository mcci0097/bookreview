package ro.ubb.bookReview.core.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Author {
    private Integer idAuthor;
    private String firstname;
    private String lastname;
    private int birth;
    private String country;
}
