package ro.ubb.bookReview.core.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Book {
    private int id;
    private String title;
    private int year;
    private String publisher;
    private Author author;
    private Genre genre;
    private Review review;
}
