package ro.ubb.bookReview.core.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.ubb.bookReview.core.model.Author;
import ro.ubb.bookReview.core.model.Book;
import ro.ubb.bookReview.core.repository.Repository;

import java.util.List;

@Service
public class BookReviewServiceImpl implements BookReviewService {

    @Autowired
    private Repository repository;

    @Override
    public List<Author> findAll() {
        return repository.findAll();
    }

/*    @Override
    public Book add(Book book) {
        return (Book) repository.save(book);
    }*/

    @Override
    public void delete(int id) {
        repository.deleteById(id);
    }

/*    @Override
    @Transactional
    public Book update(int id, Book book) {
        Optional<Book> optional = repository.findById(id);

        Book result = optional.orElse(book);
        result.setTitle(book.getTitle());
        result.setYear(book.getYear());
        result.setPublisher(book.getPublisher());

        return result;
    }*/
}
