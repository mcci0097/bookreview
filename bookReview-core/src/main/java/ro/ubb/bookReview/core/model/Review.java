package ro.ubb.bookReview.core.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Review {
    private int idBook;
    private int idUser;
    private String comment;
    private float score;
}
