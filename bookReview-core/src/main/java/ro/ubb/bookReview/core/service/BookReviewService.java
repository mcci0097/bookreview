package ro.ubb.bookReview.core.service;

import ro.ubb.bookReview.core.model.Author;
import ro.ubb.bookReview.core.model.Book;

import java.util.List;

public interface BookReviewService {

    List<Author> findAll();
    //Book add(Book book);
    void delete(int id);
    //Book update(int id, Book book);

}
