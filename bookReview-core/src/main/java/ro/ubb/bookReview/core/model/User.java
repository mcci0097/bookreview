package ro.ubb.bookReview.core.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class User {
    private int idUser;
    private Role role;
    private String firstname;
    private String lastname;
    private String username;
    private String password;
    private String email;
    private Review review;
}
