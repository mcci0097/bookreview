package ro.ubb.bookReview.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import ro.ubb.bookReview.core.model.Author;

@NoRepositoryBean
public interface Repository extends JpaRepository <Author, Integer>{
}
