package ro.ubb.bookReview.web.converter;

import ro.ubb.bookReview.web.dto.AuthorDto;
import ro.ubb.bookReview.core.model.Author;


public class AuthorConvertor extends BaseConvertor<Author, AuthorDto> {

    @Override
    public Author convertDtoToModel(AuthorDto utilizatorDto) {
        return null;
    }

    @Override
    public AuthorDto convertModelToDto(Author author) {
        AuthorDto dto = new AuthorDto(author.getIdAuthor(), author.getFirstname(),
                author.getLastname(), author.getBirth(), author.getCountry());
        return dto;
    }

}
