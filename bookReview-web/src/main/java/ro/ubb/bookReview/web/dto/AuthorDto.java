package ro.ubb.bookReview.web.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class AuthorDto {
    private Integer id;
    private String firstname;
    private String lastname;
    private Integer birth;
    private String country;
}
