package ro.ubb.bookReview.web.converter;


public interface Convertor<Model, Dto> {
    Model convertDtoToModel(Dto dto);

    Dto convertModelToDto(Model model);

}