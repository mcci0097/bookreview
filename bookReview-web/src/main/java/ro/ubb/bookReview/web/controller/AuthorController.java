package ro.ubb.bookReview.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ro.ubb.bookReview.web.converter.AuthorConvertor;
import ro.ubb.bookReview.web.dto.AuthorDto;
import ro.ubb.bookReview.core.model.Author;
import ro.ubb.bookReview.core.service.BookReviewService;

import java.util.ArrayList;
import java.util.List;

@RestController
public class AuthorController {

    @Autowired
    private BookReviewService service;

    @Autowired
    private AuthorConvertor convertor;

    @RequestMapping(value = "/authors", method = RequestMethod.GET)
    List<AuthorDto> findAllClients() {
        List<Author> list = service.findAll();
        List<AuthorDto> converted = new ArrayList<>();
        list.forEach(s -> {
            AuthorDto lalala = convertor.convertModelToDto(s);
            converted.add(lalala);
        });
        return converted;
    }

}
