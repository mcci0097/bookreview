package ro.ubb.bookReview.web.converter;

import java.util.Collection;
import java.util.stream.Collectors;

public abstract class BaseConvertor<Model, Dto>
        implements Convertor<Model, Dto> {

    public Collection<Dto> convertModelsToDtos(Collection<Model> models) {
        return models.stream()
                .map(this::convertModelToDto)
                .collect(Collectors.toSet());
    }

    public Collection<Model> convertDtosToModel(Collection<Dto> dtos) {
        return dtos.stream()
                .map(this::convertDtoToModel)
                .collect(Collectors.toSet());
    }
}